# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|
 config.vm.define "gitserver" do |gitserver|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://vagrantcloud.com/search.
  gitserver.vm.box = "ubuntu/bionic64"


  
  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # NOTE: This will enable public access to the opened port
  gitserver.vm.network "forwarded_port", guest: 3000, host: 8080
  gitserver.vm.network "forwarded_port", guest: 22, host: 8022, id: 'ssh'


  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
   gitserver.vm.provider "virtualbox" do |vb|
  #
  #   # Customize the amount of memory on the VM:
     vb.memory = "2048"
     vb.cpus = "4"
   end
# Ansible pronvisioning
  gitserver.vm.provision "ansible" do |ansible|
    ansible.playbook = "./ansible/gitserver_playbook.yml"
  end

# Synced Folders for persistance across VM destruction
  gitserver.vm.synced_folder "./conf/gitea/db/", "/var/lib/gitea/data/", create: false, mount_options: ["uid=111", "gid=116"]
  gitserver.vm.synced_folder "./conf/gitea/repos", "/var/lib/gitea/gitea-repositories", create: false, mount_options: ["uid=111", "gid=116"]
  gitserver.vm.synced_folder "./conf/gitea/app_cfg", "/etc/gitea", create: false, mount_options: ["uid=111", "gid=116"]

  end
 config.vm.define "jenkins" do |jenkins|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://vagrantcloud.com/search.
  jenkins.vm.box = "ubuntu/bionic64"


  
  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # NOTE: This will enable public access to the opened port
  jenkins.vm.network "forwarded_port", guest: 8080, host: 7080
  jenkins.vm.network "forwarded_port", guest: 22, host: 7022, id: 'ssh'


  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
   jenkins.vm.provider "virtualbox" do |vb|
  #
  #   # Customize the amount of memory on the VM:
     vb.memory = "2048"
     vb.cpus = "4"
   end
# Ansible pronvisioning
  jenkins.vm.provision "ansible" do |ansible|
    ansible.playbook = "./ansible/jenkins_playbook.yml"
  end
# Synced Folders for persistance across VM destruction
  jenkins.vm.synced_folder "./conf/jenkins", "/var/lib/jenkins", create: false, mount_options: ["uid=111", "gid=116"]
  end

  
   config.vm.define "docker" do |docker|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://vagrantcloud.com/search.
  docker.vm.box = "ubuntu/bionic64"


  
  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # NOTE: This will enable public access to the opened port
  docker.vm.network "forwarded_port", guest: 443, host: 9443
  docker.vm.network "forwarded_port", guest: 80, host: 9080
  docker.vm.network "forwarded_port", guest: 22, host: 9022, id: 'ssh'


  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
   docker.vm.provider "virtualbox" do |vb|
  #
  #   # Customize the amount of memory on the VM:
     vb.memory = "4096"
     vb.cpus = "4"
   end
# Ansible pronvisioning
docker.vm.provision "ansible" do |ansible|
    ansible.playbook = "./ansible/docker_playbook.yml"
  end
end
end