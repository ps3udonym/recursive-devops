![Logo](logo.png)


## WTF is this?
This is a contained example of a set of common devops services set up using devops tools. This seems like a novelty more than anything, but being able to write code on your desktop commit it to the locally hosted repo and then watch it go through a CI/CD process in  a closed ecosystem on your desktop seems like it could be very useful for people trying to learn devops methodology or wanting to practice a form of devops for their personal projects locally. 

## Requirements 
* Ansible
* Vagrant

## How do I run this?
Just run `vagrant up` and vagrant should take care of the rest, all the heavy lifting will be done by vagrant handling the spinning up of the virtual machines and ansible handling the configurations of the virtual machines. There is some additional configuration needed to get the services running, but these are mostly personal preferences that need to be selected at first run of these apps.


---
### Gitea
HTTP Port: 8080

SSH Port: 8022

#### Additional configuration needed:
* Ensure that the database is set to SQLlite with the default settings, you might also want to make sure the base url and base SSH port is configured to the above ports for gitea
---
### Jenkins
HTTP Port: 7080

SSH Port: 7022

#### Additional configuration needed:
* You can roll through this with the defaults and it should work okay. 
---
### Docker/Rancher
HTTP Port: 9443

SSH Port: 9022

#### Additional configuration needed:
* Set an admin password and select the option for only runnign containers on the current host.  